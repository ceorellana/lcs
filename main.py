def lcsChain(X,Y,L):
    m = len(X)
    n = len(Y)
    index = L[m][n]

    # Arreglo que tendrá la cadena mas larga
    lcs = [""] * (index + 1)
    lcs[index] = ""

    # Recorrer L de atrás hacia adelante
    i = m
    j = n
    while i > 0 and j > 0:

        # Si los caracteres actuales son iguales, son parte de la cadena mas larga
        if X[i - 1] == Y[j - 1]:
            lcs[index - 1] = X[i - 1]
            i -= 1
            j -= 1
            index -= 1

        # Si no lo son, ir por el camino mas largo según la matriz
        elif L[i - 1][j] > L[i][j - 1]:
            i -= 1
        else:
            j -= 1

    print ("Cadena mas larga: " + "".join(lcs))

def lcsSize(X , Y):
    m = len(X)
    n = len(Y)

    # Declarando matriz
    L = [[None]*(n+1) for i in range(m+1)]

    for i in range(m+1):
        for j in range(n+1):
            if i == 0 or j == 0 :
                L[i][j] = 0
            elif X[i-1] == Y[j-1]:
                L[i][j] = L[i-1][j-1]+1
            else:
                L[i][j] = max(L[i-1][j] , L[i][j-1])

    lcsChain(X,Y,L)
    # El elemento en la posicion (m,n) contiene el tamaño de la cadena mas larga
    return L[m][n]

#Programa Principal
#Abrir archivos
homoFile = open("HomoSapiens10.txt",'r')
musFile = open("MusMuscula10.txt",'r')
#Removiendo enters (\n) de los strings
cadenaH = "".join(homoFile.read().splitlines())
cadenaM = "".join(musFile.read().splitlines())
print("El tamaño de la cadena es  "+ lcsSize(cadenaH,cadenaM))
#Cerrando archivos
homoFile.close()
musFile.close()